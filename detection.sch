<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.4.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="msp430">
<packages>
<package name="RGZ48_4P1X4P1">
<smd name="1" x="-3.4798" y="2.7432" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="2" x="-3.4798" y="2.2606" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="3" x="-3.4798" y="1.7526" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="4" x="-3.4798" y="1.2446" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="5" x="-3.4798" y="0.762" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="6" x="-3.4798" y="0.254" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="7" x="-3.4798" y="-0.254" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="8" x="-3.4798" y="-0.762" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="9" x="-3.4798" y="-1.2446" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="10" x="-3.4798" y="-1.7526" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="11" x="-3.4798" y="-2.2606" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="12" x="-3.4798" y="-2.7432" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="13" x="-2.7432" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="14" x="-2.2606" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="15" x="-1.7526" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="16" x="-1.2446" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="17" x="-0.762" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="18" x="-0.254" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="19" x="0.254" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="20" x="0.762" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="21" x="1.2446" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="22" x="1.7526" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="23" x="2.2606" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="24" x="2.7432" y="-3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="25" x="3.4798" y="-2.7432" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="26" x="3.4798" y="-2.2606" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="27" x="3.4798" y="-1.7526" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="28" x="3.4798" y="-1.2446" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="29" x="3.4798" y="-0.762" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="30" x="3.4798" y="-0.254" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="31" x="3.4798" y="0.254" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="32" x="3.4798" y="0.762" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="33" x="3.4798" y="1.2446" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="34" x="3.4798" y="1.7526" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="35" x="3.4798" y="2.2606" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="36" x="3.4798" y="2.7432" dx="0.2794" dy="0.8382" layer="1" rot="R270"/>
<smd name="37" x="2.7432" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="38" x="2.2606" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="39" x="1.7526" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="40" x="1.2446" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="41" x="0.762" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="42" x="0.254" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="43" x="-0.254" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="44" x="-0.762" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="45" x="-1.2446" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="46" x="-1.7526" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="47" x="-2.2606" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="48" x="-2.7432" y="3.4798" dx="0.2794" dy="0.8382" layer="1" rot="R180"/>
<smd name="49" x="0" y="0" dx="4.0894" dy="4.0894" layer="1"/>
<wire x1="-2.9718" y1="3.8354" x2="-3.8354" y2="3.8354" width="0.1524" layer="51"/>
<wire x1="3.8354" y1="2.9718" x2="3.8354" y2="3.8354" width="0.1524" layer="51"/>
<wire x1="2.9718" y1="-3.8354" x2="3.8354" y2="-3.8354" width="0.1524" layer="51"/>
<wire x1="-3.8354" y1="-2.9718" x2="-3.8354" y2="-3.8354" width="0.1524" layer="51"/>
<wire x1="-3.8354" y1="-3.8354" x2="-2.9718" y2="-3.8354" width="0.1524" layer="51"/>
<wire x1="3.8354" y1="-3.8354" x2="3.8354" y2="-2.9718" width="0.1524" layer="51"/>
<wire x1="3.8354" y1="3.8354" x2="2.9718" y2="3.8354" width="0.1524" layer="51"/>
<wire x1="-3.8354" y1="3.8354" x2="-3.8354" y2="2.9718" width="0.1524" layer="51"/>
<wire x1="-4.4196" y1="-1.5494" x2="-4.4196" y2="-1.9304" width="0.1524" layer="51"/>
<wire x1="-4.4196" y1="-1.9304" x2="-4.1656" y2="-1.9304" width="0.1524" layer="51"/>
<wire x1="-4.1656" y1="-1.9304" x2="-4.1656" y2="-1.5494" width="0.1524" layer="51"/>
<wire x1="-4.1656" y1="-1.5494" x2="-4.4196" y2="-1.5494" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="-4.1656" x2="0.5588" y2="-4.4196" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="-4.4196" x2="0.9398" y2="-4.4196" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-4.4196" x2="0.9398" y2="-4.1656" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-4.1656" x2="0.5588" y2="-4.1656" width="0.1524" layer="51"/>
<wire x1="4.4196" y1="-0.0508" x2="4.4196" y2="-0.4318" width="0.1524" layer="51"/>
<wire x1="4.4196" y1="-0.4318" x2="4.1656" y2="-0.4318" width="0.1524" layer="51"/>
<wire x1="4.1656" y1="-0.4318" x2="4.1656" y2="-0.0508" width="0.1524" layer="51"/>
<wire x1="4.1656" y1="-0.0508" x2="4.4196" y2="-0.0508" width="0.1524" layer="51"/>
<wire x1="1.0668" y1="4.1656" x2="1.0668" y2="4.4196" width="0.1524" layer="51"/>
<wire x1="1.0668" y1="4.4196" x2="1.4478" y2="4.4196" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="4.4196" x2="1.4478" y2="4.1656" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="4.1656" x2="1.0668" y2="4.1656" width="0.1524" layer="51"/>
<wire x1="-3.8862" y1="2.8702" x2="-3.8862" y2="2.6416" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="2.6416" x2="-3.0734" y2="2.6416" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="2.6416" x2="-3.0734" y2="2.8702" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="2.8702" x2="-3.8862" y2="2.8702" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="2.3622" x2="-3.8862" y2="2.1336" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="2.1336" x2="-3.0734" y2="2.1336" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="2.1336" x2="-3.0734" y2="2.3622" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="2.3622" x2="-3.8862" y2="2.3622" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="1.8542" x2="-3.8862" y2="1.6256" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="1.6256" x2="-3.0734" y2="1.6256" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="1.6256" x2="-3.0734" y2="1.8542" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="1.8542" x2="-3.8862" y2="1.8542" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="1.3716" x2="-3.8862" y2="1.143" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="1.143" x2="-3.0734" y2="1.143" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="1.143" x2="-3.0734" y2="1.3716" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="1.3716" x2="-3.8862" y2="1.3716" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="0.8636" x2="-3.8862" y2="0.635" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="0.635" x2="-3.0734" y2="0.635" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="0.635" x2="-3.0734" y2="0.8636" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="0.8636" x2="-3.8862" y2="0.8636" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="0.3556" x2="-3.8862" y2="0.127" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="0.127" x2="-3.0734" y2="0.127" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="0.127" x2="-3.0734" y2="0.3556" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="0.3556" x2="-3.8862" y2="0.3556" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-0.127" x2="-3.8862" y2="-0.3556" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-0.3556" x2="-3.0734" y2="-0.3556" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-0.3556" x2="-3.0734" y2="-0.127" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-0.127" x2="-3.8862" y2="-0.127" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-0.635" x2="-3.8862" y2="-0.8636" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-0.8636" x2="-3.0734" y2="-0.8636" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-0.8636" x2="-3.0734" y2="-0.635" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-0.635" x2="-3.8862" y2="-0.635" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-1.143" x2="-3.8862" y2="-1.3716" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-1.3716" x2="-3.0734" y2="-1.3716" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-1.3716" x2="-3.0734" y2="-1.143" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-1.143" x2="-3.8862" y2="-1.143" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-1.6256" x2="-3.8862" y2="-1.8542" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-1.8542" x2="-3.0734" y2="-1.8542" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-1.8542" x2="-3.0734" y2="-1.6256" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-1.6256" x2="-3.8862" y2="-1.6256" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-2.1336" x2="-3.8862" y2="-2.3622" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-2.3622" x2="-3.0734" y2="-2.3622" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-2.3622" x2="-3.0734" y2="-2.1336" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-2.1336" x2="-3.8862" y2="-2.1336" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-2.6416" x2="-3.8862" y2="-2.8702" width="0.1524" layer="31"/>
<wire x1="-3.8862" y1="-2.8702" x2="-3.0734" y2="-2.8702" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-2.8702" x2="-3.0734" y2="-2.6416" width="0.1524" layer="31"/>
<wire x1="-3.0734" y1="-2.6416" x2="-3.8862" y2="-2.6416" width="0.1524" layer="31"/>
<wire x1="-2.8702" y1="-3.0734" x2="-2.8702" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-2.8702" y1="-3.8862" x2="-2.6416" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-2.6416" y1="-3.8862" x2="-2.6416" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-2.6416" y1="-3.0734" x2="-2.8702" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-2.3622" y1="-3.0734" x2="-2.3622" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-2.3622" y1="-3.8862" x2="-2.1336" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-2.1336" y1="-3.8862" x2="-2.1336" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-2.1336" y1="-3.0734" x2="-2.3622" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-1.8542" y1="-3.0734" x2="-1.8542" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-1.8542" y1="-3.8862" x2="-1.6256" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-1.6256" y1="-3.8862" x2="-1.6256" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-1.6256" y1="-3.0734" x2="-1.8542" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-1.3716" y1="-3.0734" x2="-1.3716" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-1.3716" y1="-3.8862" x2="-1.143" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-1.143" y1="-3.8862" x2="-1.143" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-1.143" y1="-3.0734" x2="-1.3716" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-0.8636" y1="-3.0734" x2="-0.8636" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-0.8636" y1="-3.8862" x2="-0.635" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-0.635" y1="-3.8862" x2="-0.635" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-0.635" y1="-3.0734" x2="-0.8636" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-0.3556" y1="-3.0734" x2="-0.3556" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-0.3556" y1="-3.8862" x2="-0.127" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="-0.127" y1="-3.8862" x2="-0.127" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="-0.127" y1="-3.0734" x2="-0.3556" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="0.127" y1="-3.0734" x2="0.127" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="0.127" y1="-3.8862" x2="0.3556" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="0.3556" y1="-3.8862" x2="0.3556" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="0.3556" y1="-3.0734" x2="0.127" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="0.635" y1="-3.0734" x2="0.635" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="0.635" y1="-3.8862" x2="0.8636" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="0.8636" y1="-3.8862" x2="0.8636" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="0.8636" y1="-3.0734" x2="0.635" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="1.143" y1="-3.0734" x2="1.143" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="1.143" y1="-3.8862" x2="1.3716" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="1.3716" y1="-3.8862" x2="1.3716" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="1.3716" y1="-3.0734" x2="1.143" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="1.6256" y1="-3.0734" x2="1.6256" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="1.6256" y1="-3.8862" x2="1.8542" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="1.8542" y1="-3.8862" x2="1.8542" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="1.8542" y1="-3.0734" x2="1.6256" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="2.1336" y1="-3.0734" x2="2.1336" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="2.1336" y1="-3.8862" x2="2.3622" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="2.3622" y1="-3.8862" x2="2.3622" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="2.3622" y1="-3.0734" x2="2.1336" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="2.6416" y1="-3.0734" x2="2.6416" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="2.6416" y1="-3.8862" x2="2.8702" y2="-3.8862" width="0.1524" layer="31"/>
<wire x1="2.8702" y1="-3.8862" x2="2.8702" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="2.8702" y1="-3.0734" x2="2.6416" y2="-3.0734" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-2.6416" x2="3.0734" y2="-2.8702" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-2.8702" x2="3.8862" y2="-2.8702" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-2.8702" x2="3.8862" y2="-2.6416" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-2.6416" x2="3.0734" y2="-2.6416" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-2.1336" x2="3.0734" y2="-2.3622" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-2.3622" x2="3.8862" y2="-2.3622" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-2.3622" x2="3.8862" y2="-2.1336" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-2.1336" x2="3.0734" y2="-2.1336" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-1.6256" x2="3.0734" y2="-1.8542" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-1.8542" x2="3.8862" y2="-1.8542" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-1.8542" x2="3.8862" y2="-1.6256" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-1.6256" x2="3.0734" y2="-1.6256" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-1.143" x2="3.0734" y2="-1.3716" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-1.3716" x2="3.8862" y2="-1.3716" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-1.3716" x2="3.8862" y2="-1.143" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-1.143" x2="3.0734" y2="-1.143" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-0.635" x2="3.0734" y2="-0.8636" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-0.8636" x2="3.8862" y2="-0.8636" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-0.8636" x2="3.8862" y2="-0.635" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-0.635" x2="3.0734" y2="-0.635" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-0.127" x2="3.0734" y2="-0.3556" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="-0.3556" x2="3.8862" y2="-0.3556" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-0.3556" x2="3.8862" y2="-0.127" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="-0.127" x2="3.0734" y2="-0.127" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="0.3556" x2="3.0734" y2="0.127" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="0.127" x2="3.8862" y2="0.127" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="0.127" x2="3.8862" y2="0.3556" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="0.3556" x2="3.0734" y2="0.3556" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="0.8636" x2="3.0734" y2="0.635" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="0.635" x2="3.8862" y2="0.635" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="0.635" x2="3.8862" y2="0.8636" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="0.8636" x2="3.0734" y2="0.8636" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="1.3716" x2="3.0734" y2="1.143" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="1.143" x2="3.8862" y2="1.143" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="1.143" x2="3.8862" y2="1.3716" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="1.3716" x2="3.0734" y2="1.3716" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="1.8542" x2="3.0734" y2="1.6256" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="1.6256" x2="3.8862" y2="1.6256" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="1.6256" x2="3.8862" y2="1.8542" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="1.8542" x2="3.0734" y2="1.8542" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="2.3622" x2="3.0734" y2="2.1336" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="2.1336" x2="3.8862" y2="2.1336" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="2.1336" x2="3.8862" y2="2.3622" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="2.3622" x2="3.0734" y2="2.3622" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="2.8702" x2="3.0734" y2="2.6416" width="0.1524" layer="31"/>
<wire x1="3.0734" y1="2.6416" x2="3.8862" y2="2.6416" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="2.6416" x2="3.8862" y2="2.8702" width="0.1524" layer="31"/>
<wire x1="3.8862" y1="2.8702" x2="3.0734" y2="2.8702" width="0.1524" layer="31"/>
<wire x1="2.6416" y1="3.8862" x2="2.6416" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="2.6416" y1="3.0734" x2="2.8702" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="2.8702" y1="3.0734" x2="2.8702" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="2.8702" y1="3.8862" x2="2.6416" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="2.1336" y1="3.8862" x2="2.1336" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="2.1336" y1="3.0734" x2="2.3622" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="2.3622" y1="3.0734" x2="2.3622" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="2.3622" y1="3.8862" x2="2.1336" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="1.6256" y1="3.8862" x2="1.6256" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="1.6256" y1="3.0734" x2="1.8542" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="1.8542" y1="3.0734" x2="1.8542" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="1.8542" y1="3.8862" x2="1.6256" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="1.143" y1="3.8862" x2="1.143" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="1.143" y1="3.0734" x2="1.3716" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="1.3716" y1="3.0734" x2="1.3716" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="1.3716" y1="3.8862" x2="1.143" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="0.635" y1="3.8862" x2="0.635" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="0.635" y1="3.0734" x2="0.8636" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="0.8636" y1="3.0734" x2="0.8636" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="0.8636" y1="3.8862" x2="0.635" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="0.127" y1="3.8862" x2="0.127" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="0.127" y1="3.0734" x2="0.3556" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="0.3556" y1="3.0734" x2="0.3556" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="0.3556" y1="3.8862" x2="0.127" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-0.3556" y1="3.8862" x2="-0.3556" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-0.3556" y1="3.0734" x2="-0.127" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-0.127" y1="3.0734" x2="-0.127" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-0.127" y1="3.8862" x2="-0.3556" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-0.8636" y1="3.8862" x2="-0.8636" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-0.8636" y1="3.0734" x2="-0.635" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-0.635" y1="3.0734" x2="-0.635" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-0.635" y1="3.8862" x2="-0.8636" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-1.3716" y1="3.8862" x2="-1.3716" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-1.3716" y1="3.0734" x2="-1.143" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-1.143" y1="3.0734" x2="-1.143" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-1.143" y1="3.8862" x2="-1.3716" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-1.8542" y1="3.8862" x2="-1.8542" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-1.8542" y1="3.0734" x2="-1.6256" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-1.6256" y1="3.0734" x2="-1.6256" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-1.6256" y1="3.8862" x2="-1.8542" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-2.3622" y1="3.8862" x2="-2.3622" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-2.3622" y1="3.0734" x2="-2.1336" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-2.1336" y1="3.0734" x2="-2.1336" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-2.1336" y1="3.8862" x2="-2.3622" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-2.8702" y1="3.8862" x2="-2.8702" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-2.8702" y1="3.0734" x2="-2.6416" y2="3.0734" width="0.1524" layer="31"/>
<wire x1="-2.6416" y1="3.0734" x2="-2.6416" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-2.6416" y1="3.8862" x2="-2.8702" y2="3.8862" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="1.9812" x2="-1.9812" y2="1.6764" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="1.6764" x2="-1.8288" y2="1.6764" width="0.1524" layer="31"/>
<wire x1="-1.6764" y1="1.9812" x2="-1.9812" y2="1.9812" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="1.4732" x2="-1.9812" y2="0.889" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="0.889" x2="-1.8288" y2="0.889" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="0.6858" x2="-1.9812" y2="0.1016" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="0.1016" x2="-1.8288" y2="0.1016" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="-0.1016" x2="-1.9812" y2="-0.6858" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="-0.6858" x2="-1.8288" y2="-0.6858" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="-0.889" x2="-1.9812" y2="-1.4732" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="-1.4732" x2="-1.8288" y2="-1.4732" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="-1.6764" x2="-1.9812" y2="-1.9812" width="0.1524" layer="31"/>
<wire x1="-1.9812" y1="-1.9812" x2="-1.6764" y2="-1.9812" width="0.1524" layer="31"/>
<wire x1="-1.6764" y1="-1.9812" x2="-1.6764" y2="-1.8288" width="0.1524" layer="31"/>
<wire x1="-1.4732" y1="1.9812" x2="-1.4732" y2="1.8288" width="0.1524" layer="31"/>
<wire x1="-0.889" y1="1.9812" x2="-1.4732" y2="1.9812" width="0.1524" layer="31"/>
<wire x1="-1.4732" y1="-1.9812" x2="-0.889" y2="-1.9812" width="0.1524" layer="31"/>
<wire x1="-0.889" y1="-1.9812" x2="-0.889" y2="-1.8288" width="0.1524" layer="31"/>
<wire x1="-0.1016" y1="1.9812" x2="-0.6858" y2="1.9812" width="0.1524" layer="31"/>
<wire x1="-0.6858" y1="-1.9812" x2="-0.1016" y2="-1.9812" width="0.1524" layer="31"/>
<wire x1="-0.1016" y1="-1.9812" x2="-0.1016" y2="-1.8288" width="0.1524" layer="31"/>
<wire x1="0.1016" y1="1.9812" x2="0.1016" y2="1.8288" width="0.1524" layer="31"/>
<wire x1="0.1016" y1="-1.9812" x2="0.6858" y2="-1.9812" width="0.1524" layer="31"/>
<wire x1="0.6858" y1="-1.9812" x2="0.6858" y2="-1.8288" width="0.1524" layer="31"/>
<wire x1="0.889" y1="-1.9812" x2="1.4732" y2="-1.9812" width="0.1524" layer="31"/>
<wire x1="1.4732" y1="-1.9812" x2="1.4732" y2="-1.8288" width="0.1524" layer="31"/>
<wire x1="1.6764" y1="1.9812" x2="1.6764" y2="1.8288" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="1.6764" x2="1.9812" y2="1.9812" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="1.9812" x2="1.6764" y2="1.9812" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="0.889" x2="1.9812" y2="1.4732" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="1.4732" x2="1.8288" y2="1.4732" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="0.1016" x2="1.9812" y2="0.6858" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="0.6858" x2="1.8288" y2="0.6858" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="-0.6858" x2="1.9812" y2="-0.1016" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="-0.1016" x2="1.8288" y2="-0.1016" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="-1.4732" x2="1.9812" y2="-0.889" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="-0.889" x2="1.8288" y2="-0.889" width="0.1524" layer="31"/>
<wire x1="1.6764" y1="-1.9812" x2="1.9812" y2="-1.9812" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="-1.9812" x2="1.9812" y2="-1.6764" width="0.1524" layer="31"/>
<wire x1="1.9812" y1="-1.6764" x2="1.8288" y2="-1.6764" width="0.1524" layer="31"/>
<wire x1="-3.9624" y1="2.9718" x2="-3.9624" y2="2.54" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="2.54" x2="-2.9718" y2="2.54" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="2.54" x2="-2.9718" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="2.9718" x2="-3.9624" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="2.4638" x2="-3.9624" y2="2.032" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="2.032" x2="-2.9718" y2="2.032" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="2.032" x2="-2.9718" y2="2.4638" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="2.4638" x2="-3.9624" y2="2.4638" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="1.9558" x2="-3.9624" y2="1.5494" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="1.5494" x2="-2.9718" y2="1.5494" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="1.5494" x2="-2.9718" y2="1.9558" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="1.9558" x2="-3.9624" y2="1.9558" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="1.4478" x2="-3.9624" y2="1.0414" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="1.0414" x2="-2.9718" y2="1.0414" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="1.0414" x2="-2.9718" y2="1.4478" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="1.4478" x2="-3.9624" y2="1.4478" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="0.9652" x2="-3.9624" y2="0.5334" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="0.5334" x2="-2.9718" y2="0.5334" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="0.5334" x2="-2.9718" y2="0.9652" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="0.9652" x2="-3.9624" y2="0.9652" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="0.4572" x2="-3.9624" y2="0.0508" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="0.0508" x2="-2.9718" y2="0.0508" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="0.0508" x2="-2.9718" y2="0.4572" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="0.4572" x2="-3.9624" y2="0.4572" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-0.0508" x2="-3.9624" y2="-0.4572" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-0.4572" x2="-2.9718" y2="-0.4572" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-0.4572" x2="-2.9718" y2="-0.0508" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-0.0508" x2="-3.9624" y2="-0.0508" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-0.5334" x2="-3.9624" y2="-0.9652" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-0.9652" x2="-2.9718" y2="-0.9652" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-0.9652" x2="-2.9718" y2="-0.5334" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-0.5334" x2="-3.9624" y2="-0.5334" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-1.0414" x2="-3.9624" y2="-1.4478" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-1.4478" x2="-2.9718" y2="-1.4478" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-1.4478" x2="-2.9718" y2="-1.0414" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-1.0414" x2="-3.9624" y2="-1.0414" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-1.5494" x2="-3.9624" y2="-1.9558" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-1.9558" x2="-2.9718" y2="-1.9558" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-1.9558" x2="-2.9718" y2="-1.5494" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-1.5494" x2="-3.9624" y2="-1.5494" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-2.032" x2="-3.9624" y2="-2.4638" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-2.4638" x2="-2.9718" y2="-2.4638" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-2.4638" x2="-2.9718" y2="-2.032" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-2.032" x2="-3.9624" y2="-2.032" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-2.54" x2="-3.9624" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-3.9624" y1="-2.9718" x2="-2.9718" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-2.9718" x2="-2.9718" y2="-2.54" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-2.54" x2="-3.9624" y2="-2.54" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-2.9718" x2="-2.9718" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="-3.9624" x2="-2.54" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-2.54" y1="-3.9624" x2="-2.54" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-2.54" y1="-2.9718" x2="-2.9718" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-2.4638" y1="-2.9718" x2="-2.4638" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-2.4638" y1="-3.9624" x2="-2.032" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-2.032" y1="-3.9624" x2="-2.032" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-2.032" y1="-2.9718" x2="-2.4638" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-1.9558" y1="-2.9718" x2="-1.9558" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-1.9558" y1="-3.9624" x2="-1.5494" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-1.5494" y1="-3.9624" x2="-1.5494" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-1.5494" y1="-2.9718" x2="-1.9558" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-1.4478" y1="-2.9718" x2="-1.4478" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-1.4478" y1="-3.9624" x2="-1.0414" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-1.0414" y1="-3.9624" x2="-1.0414" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-1.0414" y1="-2.9718" x2="-1.4478" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-0.9652" y1="-2.9718" x2="-0.9652" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-0.9652" y1="-3.9624" x2="-0.5334" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="-3.9624" x2="-0.5334" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="-2.9718" x2="-0.9652" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-0.4572" y1="-2.9718" x2="-0.4572" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-0.4572" y1="-3.9624" x2="-0.0508" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="-0.0508" y1="-3.9624" x2="-0.0508" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="-0.0508" y1="-2.9718" x2="-0.4572" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="0.0508" y1="-2.9718" x2="0.0508" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="0.0508" y1="-3.9624" x2="0.4572" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="0.4572" y1="-3.9624" x2="0.4572" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="0.4572" y1="-2.9718" x2="0.0508" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="-2.9718" x2="0.5334" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="-3.9624" x2="0.9652" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="0.9652" y1="-3.9624" x2="0.9652" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="0.9652" y1="-2.9718" x2="0.5334" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="1.0414" y1="-2.9718" x2="1.0414" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="1.0414" y1="-3.9624" x2="1.4478" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="1.4478" y1="-3.9624" x2="1.4478" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="1.4478" y1="-2.9718" x2="1.0414" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="1.5494" y1="-2.9718" x2="1.5494" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="1.5494" y1="-3.9624" x2="1.9558" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="1.9558" y1="-3.9624" x2="1.9558" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="1.9558" y1="-2.9718" x2="1.5494" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="2.032" y1="-2.9718" x2="2.032" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="2.032" y1="-3.9624" x2="2.4638" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="2.4638" y1="-3.9624" x2="2.4638" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="2.4638" y1="-2.9718" x2="2.032" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="2.54" y1="-2.9718" x2="2.54" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="2.54" y1="-3.9624" x2="2.9718" y2="-3.9624" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-3.9624" x2="2.9718" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-2.9718" x2="2.54" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-2.54" x2="2.9718" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-2.9718" x2="3.9624" y2="-2.9718" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-2.9718" x2="3.9624" y2="-2.54" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-2.54" x2="2.9718" y2="-2.54" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-2.032" x2="2.9718" y2="-2.4638" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-2.4638" x2="3.9624" y2="-2.4638" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-2.4638" x2="3.9624" y2="-2.032" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-2.032" x2="2.9718" y2="-2.032" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-1.5494" x2="2.9718" y2="-1.9558" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-1.9558" x2="3.9624" y2="-1.9558" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-1.9558" x2="3.9624" y2="-1.5494" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-1.5494" x2="2.9718" y2="-1.5494" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-1.0414" x2="2.9718" y2="-1.4478" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-1.4478" x2="3.9624" y2="-1.4478" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-1.4478" x2="3.9624" y2="-1.0414" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-1.0414" x2="2.9718" y2="-1.0414" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-0.5334" x2="2.9718" y2="-0.9652" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-0.9652" x2="3.9624" y2="-0.9652" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-0.9652" x2="3.9624" y2="-0.5334" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-0.5334" x2="2.9718" y2="-0.5334" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-0.0508" x2="2.9718" y2="-0.4572" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="-0.4572" x2="3.9624" y2="-0.4572" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-0.4572" x2="3.9624" y2="-0.0508" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="-0.0508" x2="2.9718" y2="-0.0508" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="0.4572" x2="2.9718" y2="0.0508" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="0.0508" x2="3.9624" y2="0.0508" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="0.0508" x2="3.9624" y2="0.4572" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="0.4572" x2="2.9718" y2="0.4572" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="0.9652" x2="2.9718" y2="0.5334" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="0.5334" x2="3.9624" y2="0.5334" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="0.5334" x2="3.9624" y2="0.9652" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="0.9652" x2="2.9718" y2="0.9652" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="1.4478" x2="2.9718" y2="1.0414" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="1.0414" x2="3.9624" y2="1.0414" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="1.0414" x2="3.9624" y2="1.4478" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="1.4478" x2="2.9718" y2="1.4478" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="1.9558" x2="2.9718" y2="1.5494" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="1.5494" x2="3.9624" y2="1.5494" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="1.5494" x2="3.9624" y2="1.9558" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="1.9558" x2="2.9718" y2="1.9558" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="2.4638" x2="2.9718" y2="2.032" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="2.032" x2="3.9624" y2="2.032" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="2.032" x2="3.9624" y2="2.4638" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="2.4638" x2="2.9718" y2="2.4638" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="2.9718" x2="2.9718" y2="2.54" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="2.54" x2="3.9624" y2="2.54" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="2.54" x2="3.9624" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="3.9624" y1="2.9718" x2="2.9718" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="2.54" y1="3.9624" x2="2.54" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="2.54" y1="2.9718" x2="2.9718" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="2.9718" x2="2.9718" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="2.9718" y1="3.9624" x2="2.54" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="2.032" y1="3.9624" x2="2.032" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="2.032" y1="2.9718" x2="2.4638" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="2.4638" y1="2.9718" x2="2.4638" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="2.4638" y1="3.9624" x2="2.032" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="1.5494" y1="3.9624" x2="1.5494" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="1.5494" y1="2.9718" x2="1.9558" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="1.9558" y1="2.9718" x2="1.9558" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="1.9558" y1="3.9624" x2="1.5494" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="1.0414" y1="3.9624" x2="1.0414" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="1.0414" y1="2.9718" x2="1.4478" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="1.4478" y1="2.9718" x2="1.4478" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="1.4478" y1="3.9624" x2="1.0414" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="3.9624" x2="0.5334" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="2.9718" x2="0.9652" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="0.9652" y1="2.9718" x2="0.9652" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="0.9652" y1="3.9624" x2="0.5334" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="0.0508" y1="3.9624" x2="0.0508" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="0.0508" y1="2.9718" x2="0.4572" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="0.4572" y1="2.9718" x2="0.4572" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="0.4572" y1="3.9624" x2="0.0508" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-0.4572" y1="3.9624" x2="-0.4572" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-0.4572" y1="2.9718" x2="-0.0508" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-0.0508" y1="2.9718" x2="-0.0508" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-0.0508" y1="3.9624" x2="-0.4572" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-0.9652" y1="3.9624" x2="-0.9652" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-0.9652" y1="2.9718" x2="-0.5334" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="2.9718" x2="-0.5334" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="3.9624" x2="-0.9652" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-1.4478" y1="3.9624" x2="-1.4478" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-1.4478" y1="2.9718" x2="-1.0414" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-1.0414" y1="2.9718" x2="-1.0414" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-1.0414" y1="3.9624" x2="-1.4478" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-1.9558" y1="3.9624" x2="-1.9558" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-1.9558" y1="2.9718" x2="-1.5494" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-1.5494" y1="2.9718" x2="-1.5494" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-1.5494" y1="3.9624" x2="-1.9558" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-2.4638" y1="3.9624" x2="-2.4638" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-2.4638" y1="2.9718" x2="-2.032" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-2.032" y1="2.9718" x2="-2.032" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-2.032" y1="3.9624" x2="-2.4638" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="3.9624" x2="-2.9718" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-2.9718" y1="2.9718" x2="-2.54" y2="2.9718" width="0.1524" layer="29"/>
<wire x1="-2.54" y1="2.9718" x2="-2.54" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-2.54" y1="3.9624" x2="-2.9718" y2="3.9624" width="0.1524" layer="29"/>
<wire x1="-2.1082" y1="2.1082" x2="1.778" y2="2.1082" width="0.1524" layer="29"/>
<wire x1="1.778" y1="2.1082" x2="2.1082" y2="2.1082" width="0.1524" layer="29"/>
<wire x1="2.1082" y1="1.3716" x2="2.1082" y2="0.9906" width="0.1524" layer="29"/>
<wire x1="-2.1082" y1="0.9906" x2="-2.1082" y2="1.3716" width="0.1524" layer="29"/>
<wire x1="2.1082" y1="0.5842" x2="2.1082" y2="0.2032" width="0.1524" layer="29"/>
<wire x1="-2.1082" y1="0.2032" x2="-2.1082" y2="0.5842" width="0.1524" layer="29"/>
<wire x1="2.1082" y1="-0.2032" x2="2.1082" y2="-0.5842" width="0.1524" layer="29"/>
<wire x1="-2.1082" y1="-0.5842" x2="-2.1082" y2="-0.2032" width="0.1524" layer="29"/>
<wire x1="2.1082" y1="-0.9906" x2="2.1082" y2="-1.3716" width="0.1524" layer="29"/>
<wire x1="-2.1082" y1="-1.3716" x2="-2.1082" y2="-0.9906" width="0.1524" layer="29"/>
<wire x1="-2.1082" y1="-1.778" x2="2.1082" y2="-1.778" width="0.1524" layer="29"/>
<wire x1="2.1082" y1="-1.778" x2="2.1082" y2="-2.1082" width="0.1524" layer="29"/>
<wire x1="1.778" y1="-2.1082" x2="-2.1082" y2="-2.1082" width="0.1524" layer="29"/>
<wire x1="-2.1082" y1="-2.1082" x2="-2.1082" y2="-1.778" width="0.1524" layer="29"/>
<wire x1="-2.1082" y1="-1.778" x2="-2.1082" y2="2.1082" width="0.1524" layer="29"/>
<wire x1="2.1082" y1="2.1082" x2="2.1082" y2="-1.778" width="0.1524" layer="29"/>
<wire x1="2.1082" y1="-2.1082" x2="1.778" y2="-2.1082" width="0.1524" layer="29"/>
<wire x1="1.778" y1="-2.1082" x2="1.778" y2="2.1082" width="0.1524" layer="29"/>
<wire x1="-3.5814" y1="2.3114" x2="-2.3114" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="2.8956" y1="3.5814" x2="2.5908" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="2.3876" y1="3.5814" x2="2.1082" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="1.905" y1="3.5814" x2="1.6002" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="1.397" y1="3.5814" x2="1.0922" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="0.889" y1="3.5814" x2="0.6096" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="0.4064" y1="3.5814" x2="0.1016" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="-0.1016" y1="3.5814" x2="-0.4064" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="-0.6096" y1="3.5814" x2="-0.889" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="-1.0922" y1="3.5814" x2="-1.397" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="-1.6002" y1="3.5814" x2="-1.905" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="-2.1082" y1="3.5814" x2="-2.3876" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="-2.5908" y1="3.5814" x2="-2.8956" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="2.8956" x2="-3.5814" y2="2.5908" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="2.3876" x2="-3.5814" y2="2.1082" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="1.905" x2="-3.5814" y2="1.6002" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="1.397" x2="-3.5814" y2="1.0922" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="0.889" x2="-3.5814" y2="0.6096" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="0.4064" x2="-3.5814" y2="0.1016" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="-0.1016" x2="-3.5814" y2="-0.4064" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="-0.6096" x2="-3.5814" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="-1.0922" x2="-3.5814" y2="-1.397" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="-1.6002" x2="-3.5814" y2="-1.905" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="-2.1082" x2="-3.5814" y2="-2.3876" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="-2.5908" x2="-3.5814" y2="-2.8956" width="0.1524" layer="25"/>
<wire x1="-2.8956" y1="-3.5814" x2="-2.5908" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="-2.3876" y1="-3.5814" x2="-2.1082" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="-1.905" y1="-3.5814" x2="-1.6002" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="-1.397" y1="-3.5814" x2="-1.0922" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="-0.889" y1="-3.5814" x2="-0.6096" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="-0.4064" y1="-3.5814" x2="-0.1016" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="0.1016" y1="-3.5814" x2="0.4064" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="0.6096" y1="-3.5814" x2="0.889" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="1.0922" y1="-3.5814" x2="1.397" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="1.6002" y1="-3.5814" x2="1.905" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="2.1082" y1="-3.5814" x2="2.3876" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="2.5908" y1="-3.5814" x2="2.8956" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="-2.8956" x2="3.5814" y2="-2.5908" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="-2.3876" x2="3.5814" y2="-2.1082" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="-1.905" x2="3.5814" y2="-1.6002" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="-1.397" x2="3.5814" y2="-1.0922" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="-0.889" x2="3.5814" y2="-0.6096" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="-0.4064" x2="3.5814" y2="-0.1016" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="0.1016" x2="3.5814" y2="0.4064" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="0.6096" x2="3.5814" y2="0.889" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="1.0922" x2="3.5814" y2="1.397" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="1.6002" x2="3.5814" y2="1.905" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="2.1082" x2="3.5814" y2="2.3876" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="2.5908" x2="3.5814" y2="2.8956" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="-3.5814" x2="3.5814" y2="-3.5814" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="-3.5814" x2="3.5814" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="3.5814" y1="3.5814" x2="-3.5814" y2="3.5814" width="0.1524" layer="25"/>
<wire x1="-3.5814" y1="3.5814" x2="-3.5814" y2="-3.5814" width="0.1524" layer="25"/>
<text x="-2.6202" y="4.115" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-2.8194" y="-5.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
<circle x="-3.2" y="3.2" radius="0.22360625" width="0.127" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="MSP430FR5969">
<wire x1="-35.56" y1="33.02" x2="33.02" y2="33.02" width="0.254" layer="94"/>
<wire x1="33.02" y1="33.02" x2="33.02" y2="-35.56" width="0.254" layer="94"/>
<wire x1="33.02" y1="-35.56" x2="-35.56" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-35.56" y1="-35.56" x2="-35.56" y2="33.02" width="0.254" layer="94"/>
<pin name="P1.0/TA0.1/VREF-" x="-40.64" y="12.7" length="middle"/>
<circle x="-34.036" y="31.496" radius="0.915809375" width="0.254" layer="94"/>
<pin name="P1.1/TA0.2/VEREF+" x="-40.64" y="10.16" length="middle"/>
<pin name="P1.2/TA1.1/TA0CLK" x="-40.64" y="7.62" length="middle"/>
<pin name="P3.0/A12" x="-40.64" y="5.08" length="middle"/>
<pin name="P3.1/A13" x="-40.64" y="2.54" length="middle"/>
<pin name="P3.2/A14" x="-40.64" y="0" length="middle"/>
<pin name="P3.3/A15" x="-40.64" y="-2.54" length="middle"/>
<pin name="P4.7" x="-40.64" y="-5.08" length="middle"/>
<pin name="P1.3/TA1.2" x="-40.64" y="-7.62" length="middle"/>
<pin name="P1.4/TB0.1" x="-40.64" y="-10.16" length="middle"/>
<pin name="P1.5/TB0.2" x="-40.64" y="-12.7" length="middle"/>
<pin name="PJ.0/TDO" x="-40.64" y="-15.24" length="middle"/>
<pin name="PJ.1/TDI" x="-15.24" y="-40.64" length="middle" rot="R90"/>
<pin name="PJ.2/TMS" x="-12.7" y="-40.64" length="middle" rot="R90"/>
<pin name="PJ.3/TCK" x="-10.16" y="-40.64" length="middle" rot="R90"/>
<pin name="P4.0/A8" x="-7.62" y="-40.64" length="middle" rot="R90"/>
<pin name="P4.1/A9" x="-5.08" y="-40.64" length="middle" rot="R90"/>
<pin name="P4.2/A10" x="-2.54" y="-40.64" length="middle" rot="R90"/>
<pin name="P4.3/A11" x="0" y="-40.64" length="middle" rot="R90"/>
<pin name="P2.5/TB0.0" x="2.54" y="-40.64" length="middle" rot="R90"/>
<pin name="P2.6/TB0.1" x="5.08" y="-40.64" length="middle" rot="R90"/>
<pin name="TEST/SBWTCK" x="7.62" y="-40.64" length="middle" rot="R90"/>
<pin name="RST" x="10.16" y="-40.64" length="middle" rot="R90"/>
<pin name="P2.0/TB0.6" x="12.7" y="-40.64" length="middle" rot="R90"/>
<pin name="P2.1/TB0.0" x="38.1" y="-15.24" length="middle" rot="R180"/>
<pin name="P2.2/TB0.2/UCB0CLK" x="38.1" y="-12.7" length="middle" rot="R180"/>
<pin name="P3.4/TB0.3/SMCLK" x="38.1" y="-10.16" length="middle" rot="R180"/>
<pin name="P3.5/TB0.4/COUT" x="38.1" y="-7.62" length="middle" rot="R180"/>
<pin name="P3.6/TB0.5" x="38.1" y="-5.08" length="middle" rot="R180"/>
<pin name="P3.7/TB0.6" x="38.1" y="-2.54" length="middle" rot="R180"/>
<pin name="P1.6/TB0.3/UCB0SIMO/UCB0SDA/TA0.0" x="38.1" y="0" length="middle" rot="R180"/>
<pin name="P1.7/TB0.4/UCB0SOMI/UCB0SCL/TA1.0" x="38.1" y="2.54" length="middle" rot="R180"/>
<pin name="P4.4/TB0.5" x="38.1" y="5.08" length="middle" rot="R180"/>
<pin name="P4.5" x="38.1" y="7.62" length="middle" rot="R180"/>
<pin name="P4.6" x="38.1" y="10.16" length="middle" rot="R180"/>
<pin name="DVSS" x="38.1" y="12.7" length="middle" rot="R180"/>
<pin name="DVCC" x="12.7" y="38.1" length="middle" rot="R270"/>
<pin name="P2.7" x="10.16" y="38.1" length="middle" rot="R270"/>
<pin name="P2.3/TA0.0/UCA1STE/A6/C10" x="7.62" y="38.1" length="middle" rot="R270"/>
<pin name="P2.4/TA1.0/UCA1CLK/A7/C11" x="5.08" y="38.1" length="middle" rot="R270"/>
<pin name="AVSS@1" x="2.54" y="38.1" length="middle" rot="R270"/>
<pin name="PJ.6/HFXIN" x="0" y="38.1" length="middle" rot="R270"/>
<pin name="PJ.7/HFXOUT" x="-2.54" y="38.1" length="middle" rot="R270"/>
<pin name="AVSS@2" x="-5.08" y="38.1" length="middle" rot="R270"/>
<pin name="PJ.4/LFXIN" x="-7.62" y="38.1" length="middle" rot="R270"/>
<pin name="PJ.5/LFXOUT" x="-10.16" y="38.1" length="middle" rot="R270"/>
<pin name="AVSS@3" x="-12.7" y="38.1" length="middle" rot="R270"/>
<pin name="AVCC" x="-15.24" y="38.1" length="middle" rot="R270"/>
<wire x1="-7.62" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<pin name="VSS@1" x="-2.54" y="-7.62" length="middle" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MSP430FR5969_RGZ_48" prefix="U">
<description>&lt;b&gt;MSP430FR59xx Mixed Signal Microcontroller&lt;/b&gt;&lt;br&gt;
&lt;b&gt;Package: &lt;/b&gt;RGZ-S-PQVFN-N48  (MSP430FR596x and MSP430FR586x)&lt;br&gt;&lt;br&gt;
&lt;b&gt;Description&lt;/b&gt;&lt;br&gt;
The Texas Instruments MSP430FR58xx and MSP430FR59xx family of ultralow-power microcontrollers consists of several devices featuring embedded FRAM nonvolatile memory, an ultralow-power 16-bit MSP430 CPU, and different sets of peripherals targeted for various applications. The architecture, FRAM, and peripherals, combined with seven low-power modes, are optimized to achieve extended battery life in portable and wireless sensing applications. FRAM is a new nonvolatile memory that combines the speed, flexibility, and endurance of SRAM with the stability and reliability of flash, all at lower total power consumption.</description>
<gates>
<gate name="B" symbol="MSP430FR5969" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="RGZ48_4P1X4P1">
<connects>
<connect gate="B" pin="AVCC" pad="48"/>
<connect gate="B" pin="AVSS@1" pad="41"/>
<connect gate="B" pin="AVSS@2" pad="44"/>
<connect gate="B" pin="AVSS@3" pad="47"/>
<connect gate="B" pin="DVCC" pad="37"/>
<connect gate="B" pin="DVSS" pad="36"/>
<connect gate="B" pin="P1.0/TA0.1/VREF-" pad="1"/>
<connect gate="B" pin="P1.1/TA0.2/VEREF+" pad="2"/>
<connect gate="B" pin="P1.2/TA1.1/TA0CLK" pad="3"/>
<connect gate="B" pin="P1.3/TA1.2" pad="9"/>
<connect gate="B" pin="P1.4/TB0.1" pad="10"/>
<connect gate="B" pin="P1.5/TB0.2" pad="11"/>
<connect gate="B" pin="P1.6/TB0.3/UCB0SIMO/UCB0SDA/TA0.0" pad="31"/>
<connect gate="B" pin="P1.7/TB0.4/UCB0SOMI/UCB0SCL/TA1.0" pad="32"/>
<connect gate="B" pin="P2.0/TB0.6" pad="24"/>
<connect gate="B" pin="P2.1/TB0.0" pad="25"/>
<connect gate="B" pin="P2.2/TB0.2/UCB0CLK" pad="26"/>
<connect gate="B" pin="P2.3/TA0.0/UCA1STE/A6/C10" pad="39"/>
<connect gate="B" pin="P2.4/TA1.0/UCA1CLK/A7/C11" pad="40"/>
<connect gate="B" pin="P2.5/TB0.0" pad="20"/>
<connect gate="B" pin="P2.6/TB0.1" pad="21"/>
<connect gate="B" pin="P2.7" pad="38"/>
<connect gate="B" pin="P3.0/A12" pad="4"/>
<connect gate="B" pin="P3.1/A13" pad="5"/>
<connect gate="B" pin="P3.2/A14" pad="6"/>
<connect gate="B" pin="P3.3/A15" pad="7"/>
<connect gate="B" pin="P3.4/TB0.3/SMCLK" pad="27"/>
<connect gate="B" pin="P3.5/TB0.4/COUT" pad="28"/>
<connect gate="B" pin="P3.6/TB0.5" pad="29"/>
<connect gate="B" pin="P3.7/TB0.6" pad="30"/>
<connect gate="B" pin="P4.0/A8" pad="16"/>
<connect gate="B" pin="P4.1/A9" pad="17"/>
<connect gate="B" pin="P4.2/A10" pad="18"/>
<connect gate="B" pin="P4.3/A11" pad="19"/>
<connect gate="B" pin="P4.4/TB0.5" pad="33"/>
<connect gate="B" pin="P4.5" pad="34"/>
<connect gate="B" pin="P4.6" pad="35"/>
<connect gate="B" pin="P4.7" pad="8"/>
<connect gate="B" pin="PJ.0/TDO" pad="12"/>
<connect gate="B" pin="PJ.1/TDI" pad="13"/>
<connect gate="B" pin="PJ.2/TMS" pad="14"/>
<connect gate="B" pin="PJ.3/TCK" pad="15"/>
<connect gate="B" pin="PJ.4/LFXIN" pad="45"/>
<connect gate="B" pin="PJ.5/LFXOUT" pad="46"/>
<connect gate="B" pin="PJ.6/HFXIN" pad="42"/>
<connect gate="B" pin="PJ.7/HFXOUT" pad="43"/>
<connect gate="B" pin="RST" pad="23"/>
<connect gate="B" pin="TEST/SBWTCK" pad="22"/>
<connect gate="B" pin="VSS@1" pad="49"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="msp430fr5969_rgz_48" constant="no"/>
<attribute name="VENDOR" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X04">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<wire x1="-1.75" y1="3.4" x2="12.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.25" y1="3.4" x2="12.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-2.8" x2="12.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.25" y1="3.15" x2="12.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="12.75" y1="3.15" x2="12.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="12.75" y1="2.15" x2="12.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1.27MM">
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
</package>
<package name="MOLEX-1X4_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
</package>
<package name="1X04-SMD">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="1X04_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-4-PTH">
<pad name="1" x="-3" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="0" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="0" drill="0.7" diameter="1.6"/>
<text x="-2.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="-3.4" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<text x="0.7" y="0.9" size="0.8" layer="51">S</text>
<text x="2.7" y="0.9" size="0.8" layer="51">S</text>
<wire x1="-4.95" y1="-1.6" x2="-4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="6" x2="4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="6" x2="4.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-1.6" x2="-4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-1.6" x2="4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.6" x2="-4.3" y2="0" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.6" x2="4.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1MM-RA">
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X04_SMD_STRAIGHT_COMBO">
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-SMD_LONG">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="JST-4-PTH-VERT">
<wire x1="-4.95" y1="-2.25" x2="-4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-4.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-3" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="1" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="-1.4" y="0.75" size="1.27" layer="51">+</text>
<text x="0.6" y="0.75" size="1.27" layer="51">-</text>
<text x="2.7" y="0.95" size="0.8" layer="51">Y</text>
<text x="-3.3" y="0.95" size="0.8" layer="51">B</text>
</package>
<package name="1X04_SMD_RA_FEMALE">
<wire x1="-5.205" y1="4.25" x2="-5.205" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="4.25" x2="-5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="-4.25" x2="5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-5.205" y1="-4.25" x2="5.205" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-1.59" y1="6.8" x2="-0.95" y2="7.65" layer="51"/>
<rectangle x1="0.95" y1="6.8" x2="1.59" y2="7.65" layer="51"/>
<rectangle x1="-4.13" y1="6.8" x2="-3.49" y2="7.65" layer="51"/>
<smd name="3" x="1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="-1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-4.425" y="2.775" size="1" layer="27">&gt;Value</text>
<text x="-4.225" y="-3.395" size="1" layer="25">&gt;Name</text>
<rectangle x1="3.49" y1="6.8" x2="4.13" y2="7.65" layer="51"/>
<smd name="4" x="3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
</package>
<package name="1X04-1.5MM_JST">
<pad name="4" x="4.5" y="0" drill="0.7"/>
<pad name="3" x="3" y="0" drill="0.7"/>
<pad name="2" x="1.5" y="0" drill="0.7"/>
<pad name="1" x="0" y="0" drill="0.7"/>
<text x="-0.508" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-1.5" y1="2.2" x2="6" y2="2.2" width="0.3048" layer="21"/>
<wire x1="6" y1="2.2" x2="6" y2="-1.5" width="0.3048" layer="21"/>
<wire x1="6" y1="-1.5" x2="4.5" y2="-1.5" width="0.3048" layer="21"/>
<wire x1="4.5" y1="-1.5" x2="4.5" y2="-1" width="0.3048" layer="21"/>
<wire x1="4.5" y1="-1" x2="0" y2="-1" width="0.3048" layer="21"/>
<wire x1="0" y1="-1" x2="0" y2="-1.5" width="0.3048" layer="21"/>
<wire x1="0" y1="-1.5" x2="-1.5" y2="-1.5" width="0.3048" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="2.2" width="0.3048" layer="21"/>
</package>
<package name="1X04_NO_SILK_ALL_ROUND">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="2X4">
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M04X2">
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<text x="-4.572" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-4.064" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M04" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 4&lt;/b&gt;
Standard 4-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="Combine 2x8399" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.27MM" package="1X04-1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X04-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09916" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="1X04-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08511"/>
<attribute name="VALUE" value="1X04_SMD_STRAIGHT_COMBO"/>
</technology>
</technologies>
</device>
<device name="SMD_LONG" package="1X04-SMD_LONG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-4-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X04_SMD_RA_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST" package="1X04-1.5MM_JST">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_SILK_ALL_ROUND" package="1X04_NO_SILK_ALL_ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M04X2" prefix="JP" uservalue="yes">
<description>.1" header, two rows of four.</description>
<gates>
<gate name="G$1" symbol="M04X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-amp-quick">
<description>&lt;b&gt;AMP Connectors, Type QUICK&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="04P">
<description>&lt;b&gt;AMP QUICK CONNECTOR&lt;/b&gt;</description>
<wire x1="-2.921" y1="1.778" x2="-2.794" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="1.651" x2="-2.54" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.397" x2="-2.286" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.651" x2="-2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-2.159" x2="-2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-2.286" x2="-4.826" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-2.159" x2="-2.794" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-2.159" x2="-2.286" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.54" x2="-4.826" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="1.778" x2="-4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.778" x2="-4.826" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.778" x2="-4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.778" x2="-3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.778" x2="-4.445" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.524" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.778" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.524" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.524" x2="-2.794" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.524" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.524" x2="-4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.524" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="-4.826" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.54" x2="-4.064" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="2.54" x2="-4.064" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="2.54" x2="-3.556" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="2.667" x2="-4.064" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.54" x2="-1.524" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.54" x2="-1.016" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="2.413" x2="-2.286" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.413" x2="-2.794" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-2.159" x2="-0.254" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-2.159" x2="0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-2.286" x2="0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.778" x2="1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.778" x2="-0.635" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.524" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.778" x2="0.635" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.524" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.524" x2="-0.254" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.524" x2="0.254" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-1.524" x2="0.635" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.524" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.54" x2="1.016" y2="2.667" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.54" x2="2.286" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.54" x2="1.524" y2="2.667" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.667" x2="1.016" y2="2.667" width="0.1524" layer="21"/>
<wire x1="0.254" y1="2.413" x2="0.254" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.254" y1="2.54" x2="1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.254" y1="2.413" x2="-0.254" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="2.413" x2="-0.254" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.54" x2="-0.254" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.667" x2="-1.524" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="2.54" x2="-1.524" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.413" x2="-2.286" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.54" x2="-3.556" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.778" x2="-4.826" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.778" x2="-0.254" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.778" x2="0.254" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.651" x2="-2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="1.651" x2="-2.794" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.778" x2="-0.254" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.254" y1="1.778" x2="0.254" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.778" x2="-3.175" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-2.286" x2="-0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.778" x2="0.635" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-2.159" x2="-0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.778" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-1.778" x2="4.826" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-2.159" x2="2.286" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-2.159" x2="2.286" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-2.159" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-1.778" x2="4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.778" x2="4.445" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.524" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.778" x2="3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.524" x2="2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.524" x2="2.794" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-1.524" x2="3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.524" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.794" y1="1.651" x2="2.794" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.651" x2="2.54" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.651" x2="2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.286" y2="1.651" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.397" x2="2.921" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="4.826" y2="1.778" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.778" x2="3.175" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-1.778" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.286" y1="2.413" x2="2.286" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.413" x2="2.286" y2="2.413" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.54" x2="3.556" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.413" x2="2.794" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.556" y1="2.54" x2="3.556" y2="2.667" width="0.1524" layer="21"/>
<wire x1="4.064" y1="2.667" x2="3.556" y2="2.667" width="0.1524" layer="21"/>
<wire x1="4.064" y1="2.54" x2="4.064" y2="2.667" width="0.1524" layer="21"/>
<wire x1="4.064" y1="2.54" x2="4.826" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.54" x2="4.826" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.254" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="0.9144" shape="long" rot="R90"/>
<text x="-3.302" y="0.9398" size="0.9906" layer="21" ratio="12" rot="R90">1</text>
<text x="-4.826" y="2.9464" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.8354" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.1402" y1="-0.3302" x2="-3.4798" y2="0.3302" layer="51"/>
<rectangle x1="-1.6002" y1="-0.3302" x2="-0.9398" y2="0.3302" layer="51"/>
<rectangle x1="0.9398" y1="-0.3302" x2="1.6002" y2="0.3302" layer="51"/>
<rectangle x1="3.4798" y1="-0.3302" x2="4.1402" y2="0.3302" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M04" prefix="SL" uservalue="yes">
<description>&lt;b&gt;AMP QUICK CONNECTOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="04P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rf_connect">
<packages>
<package name="2X4">
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="EX_NRF24L01+">
<wire x1="-10.16" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<pin name="GND" x="-15.24" y="10.16" length="middle"/>
<pin name="CE" x="-15.24" y="5.08" length="middle"/>
<pin name="SCK" x="-15.24" y="0" length="middle"/>
<pin name="MISO" x="-15.24" y="-5.08" length="middle"/>
<pin name="IRQ" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="MOSI" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="!CS" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="VCC" x="15.24" y="10.16" length="middle" rot="R180"/>
<text x="-9.906" y="13.208" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="EX_NRF24L01+" prefix="U">
<gates>
<gate name="G$1" symbol="EX_NRF24L01+" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="2X4">
<connects>
<connect gate="G$1" pin="!CS" pad="4"/>
<connect gate="G$1" pin="CE" pad="3"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IRQ" pad="8"/>
<connect gate="G$1" pin="MISO" pad="7"/>
<connect gate="G$1" pin="MOSI" pad="6"/>
<connect gate="G$1" pin="SCK" pad="5"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.6002" y1="0.6858" x2="1.6002" y2="0.6858" width="0.0508" layer="39"/>
<wire x1="1.6002" y1="0.6858" x2="1.6002" y2="-0.6858" width="0.0508" layer="39"/>
<wire x1="1.6002" y1="-0.6858" x2="-1.6002" y2="-0.6858" width="0.0508" layer="39"/>
<wire x1="-1.6002" y1="-0.6858" x2="-1.6002" y2="0.6858" width="0.0508" layer="39"/>
<rectangle x1="-0.1905" y1="-0.3048" x2="0.1905" y2="0.3048" layer="21"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
<package name="AXIAL-0.1">
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.54" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="0" y="-2.159" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.1EZ">
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="2.54" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="0" y="-2.159" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="0" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="0" y="0" radius="1.016" width="0" layer="30"/>
<circle x="2.54" y="0" radius="1.016" width="0" layer="30"/>
<circle x="0" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="0603-RES">
<wire x1="-1.6002" y1="0.6858" x2="1.6002" y2="0.6858" width="0.0508" layer="39"/>
<wire x1="1.6002" y1="0.6858" x2="1.6002" y2="-0.6858" width="0.0508" layer="39"/>
<wire x1="1.6002" y1="-0.6858" x2="-1.6002" y2="-0.6858" width="0.0508" layer="39"/>
<wire x1="-1.6002" y1="-0.6858" x2="-1.6002" y2="0.6858" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1.5KOHM-1/10W-1%(0603)" prefix="R" uservalue="yes">
<description>RES-08306</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08306" constant="no"/>
<attribute name="VALUE" value="1.5KOHM" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10KOHM-1/4W-1%(PTH)" prefix="R" uservalue="yes">
<description>RES-12183</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="HORIZ" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="HORIZ-KIT" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="VERT" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="VERT-KIT" package="AXIAL-0.1EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="4.7KOHM-1/10W-1%(0603)" prefix="R" uservalue="yes">
<description>RES-07857</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-07857" constant="no"/>
<attribute name="VALUE" value="4.7K" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="33OHM1/10W1%(0603)" prefix="R" uservalue="yes">
<description>RES-08270</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08270" constant="no"/>
<attribute name="VALUE" value="33" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fc-135">
<packages>
<package name="FC-135_1PRIMARY">
<description>Original name &lt;b&gt;FC-135&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-1.2446" y="0" dx="0.9906" dy="1.8034" layer="1"/>
<smd name="2" x="1.2446" y="0" dx="0.9906" dy="1.8034" layer="1"/>
<wire x1="-0.762" y1="0.762" x2="0.762" y2="0.762" width="0.254" layer="21"/>
<wire x1="-0.762" y1="-0.762" x2="0.762" y2="-0.762" width="0.254" layer="21"/>
<text x="-1.851609375" y="1.255596875" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.851609375" y="1.001596875" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="FC-135">
<wire x1="0" y1="1.016" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-0.381" x2="1.524" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-0.381" x2="1.524" y2="0.381" width="0.1524" layer="94"/>
<wire x1="1.524" y1="0.381" x2="-1.524" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0.381" x2="-1.524" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="1.016" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="1.016" x2="1.778" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-1.016" x2="0" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.016" x2="1.778" y2="-1.016" width="0.1524" layer="94"/>
<text x="-1.016" y="3.048" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="96">&gt;Value</text>
<pin name="1" x="5.08" y="-2.54" length="middle" rot="R180"/>
<pin name="2" x="5.08" y="2.54" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FC-135">
<gates>
<gate name="G$1" symbol="FC-135" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FC-135_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.5588" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2.2NF/2200PF-50V-10%(0603)" prefix="C" uservalue="yes">
<description>CAP-07877</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-07877" constant="no"/>
<attribute name="VALUE" value="2.2nF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="100UF6.3V20%(1210)" prefix="C" uservalue="yes">
<description>CAP-09827</description>
<gates>
<gate name="G$1" symbol="CAP" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09827" constant="no"/>
<attribute name="VALUE" value="100uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.1UF-16V(+-10%)(0402)" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12416"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find discrete LEDs for illumination or indication, but no displays.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-1206">
<wire x1="-1" y1="1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="1" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="0.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0.6" x2="-0.3" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="0" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="-0.889" y="1.397" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.778" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED-0603">
<wire x1="0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="-0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="0" y1="0.17" x2="0.2338" y2="-0.14" width="0.2032" layer="21"/>
<wire x1="-0.0254" y1="0.1546" x2="-0.2184" y2="-0.14" width="0.2032" layer="21"/>
<smd name="C" x="0" y="0.877" dx="1" dy="1" layer="1" roundness="30"/>
<smd name="A" x="0" y="-0.877" dx="1" dy="1" layer="1" roundness="30"/>
<text x="-0.6985" y="-0.889" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="1.0795" y="-1.016" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="LED10MM">
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7" shape="square"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
</package>
<package name="FKIT-LED-1206">
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="0.5" x2="-0.55" y2="0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LED3MM-NS">
<description>&lt;h3&gt;LED 3MM - No Silk&lt;/h3&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM-KIT">
<description>&lt;h3&gt;LED5MM-KIT&lt;/h3&gt;
5MM Through-hole LED&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="LED-1206-BOTTOM">
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<wire x1="1.016" y1="1.016" x2="2.7686" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.7686" y1="-1.016" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="3.175" y1="0" x2="3.3528" y2="0" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-1.016" x2="-2.7686" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.7686" y1="-1.016" x2="-2.7686" y2="1.016" width="0.127" layer="21"/>
<wire x1="-2.7686" y1="1.016" x2="-1.016" y2="1.016" width="0.127" layer="21"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.175" y1="0" x2="-3.81" y2="0" width="0.254" layer="21"/>
<wire x1="-3.48741875" y1="-0.368296875" x2="-3.48741875" y2="0.3556" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.81" y2="0" width="0.254" layer="21"/>
<wire x1="-3.489959375" y1="0.37591875" x2="-3.48741875" y2="0.373378125" width="0.254" layer="21"/>
<wire x1="-3.48741875" y1="0.373378125" x2="-3.48741875" y2="-0.370840625" width="0.254" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D" uservalue="yes">
<description>&lt;b&gt;LEDs&lt;/b&gt;
Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. 5mm - Spark Fun Electronics SKU : COM-00529 (and others)</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<packages>
</packages>
<symbols>
<symbol name="LETTER_L">
<frame x1="0" y1="0" x2="248.92" y2="185.42" columns="12" rows="17" layer="94"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LETTER_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
LETTER landscape</description>
<gates>
<gate name="G$1" symbol="LETTER_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="147.32" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ncv551_rob">
<packages>
<package name="NCV551">
<smd name="P$1" x="0" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="-0.95" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$3" x="-1.9" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$4" x="-1.9" y="-2.4" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$5" x="0" y="-2.4" dx="1" dy="0.7" layer="1" rot="R90"/>
<text x="-2.54" y="-1.94" size="0.4064" layer="21" rot="R90">NCV551</text>
<wire x1="0.6" y1="-0.5" x2="-2.5" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-0.5" x2="-2.5" y2="-1.9" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-1.9" x2="0.6" y2="-1.9" width="0.127" layer="51"/>
<wire x1="0.6" y1="-1.9" x2="0.6" y2="-0.5" width="0.127" layer="51"/>
<circle x="0" y="-0.889" radius="0.127" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="NCV551">
<wire x1="-10.16" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<pin name="VIN" x="-7.62" y="-10.16" length="middle" rot="R90"/>
<pin name="GND" x="0" y="10.16" length="middle" rot="R270"/>
<pin name="ENABLE" x="-7.62" y="10.16" length="middle" rot="R270"/>
<pin name="N/C" x="7.62" y="10.16" length="middle" rot="R270"/>
<pin name="VOUT" x="7.62" y="-10.16" length="middle" rot="R90"/>
<text x="12.7" y="2.54" size="1.27" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NCV551">
<gates>
<gate name="G$1" symbol="NCV551" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NCV551">
<connects>
<connect gate="G$1" pin="ENABLE" pad="P$1"/>
<connect gate="G$1" pin="GND" pad="P$2"/>
<connect gate="G$1" pin="N/C" pad="P$3"/>
<connect gate="G$1" pin="VIN" pad="P$4"/>
<connect gate="G$1" pin="VOUT" pad="P$5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="msp430" deviceset="MSP430FR5969_RGZ_48" device=""/>
<part name="JP1" library="SparkFun-Connectors" deviceset="M04" device="PTH"/>
<part name="SL1" library="con-amp-quick" deviceset="M04" device=""/>
<part name="U2" library="rf_connect" deviceset="EX_NRF24L01+" device=""/>
<part name="GND1" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND4" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R1" library="SparkFun-Resistors" deviceset="1.5KOHM-1/10W-1%(0603)" device="" value="1.5KOHM"/>
<part name="R2" library="SparkFun-Resistors" deviceset="10KOHM-1/4W-1%(PTH)" device="HORIZ-KIT" value="10k"/>
<part name="GND5" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND7" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND6" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND8" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="KP" library="SparkFun-Connectors" deviceset="M04X2" device=""/>
<part name="XTA" library="fc-135" deviceset="FC-135" device=""/>
<part name="R3" library="SparkFun-Resistors" deviceset="4.7KOHM-1/10W-1%(0603)" device="" value="4.7K"/>
<part name="C1" library="SparkFun-Capacitors" deviceset="2.2NF/2200PF-50V-10%(0603)" device="" value="2.2nF"/>
<part name="GND9" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C2" library="SparkFun-Capacitors" deviceset="100UF6.3V20%(1210)" device="" value="100uF"/>
<part name="C3" library="SparkFun-Capacitors" deviceset="0.1UF-16V(+-10%)(0402)" device="" value="0.1uF"/>
<part name="GND10" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="D1" library="SparkFun-LED" deviceset="LED" device="-FKIT-1206"/>
<part name="R4" library="SparkFun-Resistors" deviceset="33OHM1/10W1%(0603)" device="" value="330"/>
<part name="GND11" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" deviceset="LETTER_L" device=""/>
<part name="C4" library="SparkFun-Capacitors" deviceset="100UF6.3V20%(1210)" device="" value="100uF"/>
<part name="C5" library="SparkFun-Capacitors" deviceset="0.1UF-16V(+-10%)(0402)" device="" value="0.1uF"/>
<part name="GND12" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="U$1" library="ncv551_rob" deviceset="NCV551" device=""/>
<part name="GND3" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<frame x1="-863.6" y1="127" x2="-254" y2="584.2" columns="8" rows="5" layer="91"/>
<text x="-477.52" y="353.06" size="1.778" layer="91">MSP430FR5969</text>
<text x="-828.04" y="561.34" size="1.778" layer="91">Power/Programming</text>
<text x="-828.04" y="518.16" size="1.778" layer="91">Passive IR I/O</text>
<text x="-528.32" y="558.8" size="1.778" layer="91">RF Communications</text>
<text x="-830.58" y="459.74" size="1.778" layer="91">Keypad Input</text>
<text x="-718.82" y="462.28" size="1.778" layer="91">Bypass (3.3V)</text>
<text x="-358.14" y="156.464" size="3.81" layer="91">Alarm Detecion Unit
By Robert Townsend</text>
<text x="-721.36" y="508" size="1.778" layer="91">Bypass (9V)</text>
</plain>
<instances>
<instance part="U1" gate="B" x="-436.88" y="317.5"/>
<instance part="JP1" gate="G$1" x="-830.58" y="551.18"/>
<instance part="SL1" gate="G$1" x="-830.58" y="505.46"/>
<instance part="U2" gate="G$1" x="-518.16" y="535.94"/>
<instance part="GND1" gate="1" x="-817.88" y="541.02"/>
<instance part="GND2" gate="1" x="-822.96" y="505.46" rot="R90"/>
<instance part="GND4" gate="1" x="-535.94" y="546.1" rot="R270"/>
<instance part="R1" gate="G$1" x="-820.42" y="495.3"/>
<instance part="R2" gate="G$1" x="-518.16" y="304.8"/>
<instance part="GND5" gate="1" x="-449.58" y="358.14" rot="R180"/>
<instance part="GND7" gate="1" x="-396.24" y="330.2" rot="R90"/>
<instance part="GND6" gate="1" x="-434.34" y="358.14" rot="R180"/>
<instance part="GND8" gate="1" x="-441.96" y="358.14" rot="R180"/>
<instance part="KP" gate="G$1" x="-800.1" y="457.2" rot="R90"/>
<instance part="XTA" gate="G$1" x="-454.66" y="373.38"/>
<instance part="R3" gate="G$1" x="-388.62" y="271.78"/>
<instance part="C1" gate="G$1" x="-401.32" y="261.62"/>
<instance part="GND9" gate="1" x="-401.32" y="256.54"/>
<instance part="C2" gate="G$1" x="-721.36" y="447.04"/>
<instance part="C3" gate="G$1" x="-708.66" y="447.04"/>
<instance part="GND10" gate="1" x="-721.36" y="434.34"/>
<instance part="D1" gate="G$1" x="-396.24" y="365.76" rot="R90"/>
<instance part="R4" gate="G$1" x="-403.86" y="365.76"/>
<instance part="GND11" gate="1" x="-388.62" y="365.76" rot="R90"/>
<instance part="FRAME1" gate="G$2" x="-360.68" y="132.08"/>
<instance part="C4" gate="G$1" x="-726.44" y="492.76"/>
<instance part="C5" gate="G$1" x="-713.74" y="492.76"/>
<instance part="GND12" gate="1" x="-726.44" y="477.52"/>
<instance part="U$1" gate="G$1" x="-640.08" y="535.94"/>
<instance part="GND3" gate="1" x="-640.08" y="548.64" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="BATT+" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="4"/>
<wire x1="-825.5" y1="556.26" x2="-805.18" y2="556.26" width="0.1524" layer="91"/>
<label x="-802.64" y="556.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-815.34" y1="495.3" x2="-810.26" y2="495.3" width="0.1524" layer="91"/>
<label x="-810.26" y="495.3" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-741.68" y1="505.46" x2="-726.44" y2="505.46" width="0.1524" layer="91"/>
<junction x="-726.44" y="505.46"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-726.44" y1="505.46" x2="-726.44" y2="497.84" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="-726.44" y1="505.46" x2="-713.74" y2="505.46" width="0.1524" layer="91"/>
<wire x1="-713.74" y1="505.46" x2="-713.74" y2="497.84" width="0.1524" layer="91"/>
<label x="-741.68" y="505.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="ENABLE"/>
<wire x1="-647.7" y1="546.1" x2="-678.18" y2="546.1" width="0.1524" layer="91"/>
<label x="-678.18" y="546.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="N/C"/>
<wire x1="-632.46" y1="546.1" x2="-609.6" y2="546.1" width="0.1524" layer="91"/>
<label x="-609.6" y="546.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="RST'" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="3"/>
<wire x1="-825.5" y1="553.72" x2="-805.18" y2="553.72" width="0.1524" layer="91"/>
<label x="-802.64" y="553.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="RST"/>
<wire x1="-426.72" y1="276.86" x2="-426.72" y2="271.78" width="0.1524" layer="91"/>
<wire x1="-426.72" y1="271.78" x2="-401.32" y2="271.78" width="0.1524" layer="91"/>
<junction x="-401.32" y="271.78"/>
<wire x1="-401.32" y1="271.78" x2="-401.32" y2="276.86" width="0.1524" layer="91"/>
<label x="-401.32" y="276.86" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-401.32" y1="271.78" x2="-393.7" y2="271.78" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-401.32" y1="271.78" x2="-401.32" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TEST" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="-825.5" y1="551.18" x2="-805.18" y2="551.18" width="0.1524" layer="91"/>
<label x="-802.64" y="551.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="TEST/SBWTCK"/>
<wire x1="-429.26" y1="276.86" x2="-429.26" y2="251.46" width="0.1524" layer="91"/>
<label x="-429.26" y="251.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="ALARM" class="0">
<segment>
<pinref part="SL1" gate="G$1" pin="3"/>
<wire x1="-825.5" y1="508" x2="-812.8" y2="508" width="0.1524" layer="91"/>
<label x="-812.8" y="508" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-523.24" y1="304.8" x2="-533.4" y2="304.8" width="0.1524" layer="91"/>
<label x="-533.4" y="304.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="-825.5" y1="548.64" x2="-817.88" y2="548.64" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-817.88" y1="543.56" x2="-817.88" y2="548.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SL1" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="DVSS"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="AVSS@3"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="AVSS@1"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="AVSS@2"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-721.36" y1="444.5" x2="-721.36" y2="439.42" width="0.1524" layer="91"/>
<junction x="-721.36" y="439.42"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="-721.36" y1="439.42" x2="-721.36" y2="436.88" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-721.36" y1="439.42" x2="-708.66" y2="439.42" width="0.1524" layer="91"/>
<wire x1="-708.66" y1="439.42" x2="-708.66" y2="444.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="-726.44" y1="490.22" x2="-726.44" y2="485.14" width="0.1524" layer="91"/>
<junction x="-726.44" y="485.14"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="-726.44" y1="485.14" x2="-726.44" y2="480.06" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="-713.74" y1="490.22" x2="-713.74" y2="485.14" width="0.1524" layer="91"/>
<wire x1="-713.74" y1="485.14" x2="-726.44" y2="485.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="SL1" gate="G$1" pin="1"/>
<wire x1="-825.5" y1="502.92" x2="-825.5" y2="495.3" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="-502.92" y1="546.1" x2="-502.92" y2="553.72" width="0.1524" layer="91"/>
<label x="-502.92" y="553.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="AVCC"/>
<wire x1="-452.12" y1="355.6" x2="-462.28" y2="355.6" width="0.1524" layer="91"/>
<wire x1="-462.28" y1="355.6" x2="-462.28" y2="360.68" width="0.1524" layer="91"/>
<label x="-462.28" y="360.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="DVCC"/>
<wire x1="-424.18" y1="355.6" x2="-416.56" y2="355.6" width="0.1524" layer="91"/>
<label x="-416.56" y="355.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-383.54" y1="271.78" x2="-383.54" y2="276.86" width="0.1524" layer="91"/>
<label x="-383.54" y="276.86" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-736.6" y1="457.2" x2="-721.36" y2="457.2" width="0.1524" layer="91"/>
<junction x="-721.36" y="457.2"/>
<label x="-736.6" y="457.2" size="1.778" layer="95"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-721.36" y1="457.2" x2="-721.36" y2="452.12" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-721.36" y1="457.2" x2="-708.66" y2="457.2" width="0.1524" layer="91"/>
<wire x1="-708.66" y1="457.2" x2="-708.66" y2="452.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VOUT"/>
<wire x1="-632.46" y1="525.78" x2="-612.14" y2="525.78" width="0.1524" layer="91"/>
<label x="-612.14" y="525.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="RF_MISO" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="MISO"/>
<wire x1="-533.4" y1="530.86" x2="-543.56" y2="530.86" width="0.1524" layer="91"/>
<label x="-543.56" y="528.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P1.6/TB0.3/UCB0SIMO/UCB0SDA/TA0.0"/>
<wire x1="-398.78" y1="317.5" x2="-388.62" y2="317.5" width="0.1524" layer="91"/>
<label x="-388.62" y="317.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="RF_SCK" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SCK"/>
<wire x1="-533.4" y1="535.94" x2="-543.56" y2="535.94" width="0.1524" layer="91"/>
<label x="-543.56" y="533.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P2.2/TB0.2/UCB0CLK"/>
<wire x1="-398.78" y1="304.8" x2="-391.16" y2="304.8" width="0.1524" layer="91"/>
<label x="-391.16" y="304.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="RF_CE" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="CE"/>
<wire x1="-533.4" y1="541.02" x2="-543.56" y2="541.02" width="0.1524" layer="91"/>
<label x="-543.56" y="538.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P1.3/TA1.2"/>
<wire x1="-477.52" y1="309.88" x2="-490.22" y2="309.88" width="0.1524" layer="91"/>
<label x="-495.3" y="309.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="RF_CS'" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="!CS"/>
<wire x1="-502.92" y1="541.02" x2="-495.3" y2="541.02" width="0.1524" layer="91"/>
<label x="-495.3" y="538.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P4.4/TB0.5"/>
<wire x1="-398.78" y1="322.58" x2="-391.16" y2="322.58" width="0.1524" layer="91"/>
<label x="-391.16" y="322.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="RF_MOSI" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="MOSI"/>
<wire x1="-502.92" y1="535.94" x2="-495.3" y2="535.94" width="0.1524" layer="91"/>
<label x="-495.3" y="533.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P1.7/TB0.4/UCB0SOMI/UCB0SCL/TA1.0"/>
<wire x1="-398.78" y1="320.04" x2="-391.16" y2="320.04" width="0.1524" layer="91"/>
<label x="-391.16" y="320.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="RF_IRQ" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IRQ"/>
<wire x1="-502.92" y1="530.86" x2="-495.3" y2="530.86" width="0.1524" layer="91"/>
<label x="-495.3" y="528.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P4.5"/>
<wire x1="-398.78" y1="325.12" x2="-391.16" y2="325.12" width="0.1524" layer="91"/>
<label x="-391.16" y="325.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="KP1" class="0">
<segment>
<pinref part="KP" gate="G$1" pin="1"/>
<wire x1="-805.18" y1="449.58" x2="-815.34" y2="449.58" width="0.1524" layer="91"/>
<label x="-815.34" y="447.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P1.0/TA0.1/VREF-"/>
<wire x1="-477.52" y1="330.2" x2="-530.86" y2="330.2" width="0.1524" layer="91"/>
<label x="-530.86" y="330.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="KP3" class="0">
<segment>
<pinref part="KP" gate="G$1" pin="3"/>
<wire x1="-802.64" y1="449.58" x2="-802.64" y2="444.5" width="0.1524" layer="91"/>
<wire x1="-802.64" y1="444.5" x2="-815.34" y2="444.5" width="0.1524" layer="91"/>
<label x="-815.34" y="441.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P1.2/TA1.1/TA0CLK"/>
<wire x1="-477.52" y1="325.12" x2="-530.86" y2="325.12" width="0.1524" layer="91"/>
<label x="-530.86" y="325.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="KP5" class="0">
<segment>
<pinref part="KP" gate="G$1" pin="5"/>
<wire x1="-800.1" y1="449.58" x2="-800.1" y2="439.42" width="0.1524" layer="91"/>
<wire x1="-800.1" y1="439.42" x2="-815.34" y2="439.42" width="0.1524" layer="91"/>
<label x="-815.34" y="436.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P3.1/A13"/>
<wire x1="-477.52" y1="320.04" x2="-530.86" y2="320.04" width="0.1524" layer="91"/>
<label x="-530.86" y="320.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="KP7" class="0">
<segment>
<pinref part="KP" gate="G$1" pin="7"/>
<wire x1="-797.56" y1="449.58" x2="-797.56" y2="434.34" width="0.1524" layer="91"/>
<wire x1="-797.56" y1="434.34" x2="-815.34" y2="434.34" width="0.1524" layer="91"/>
<label x="-815.34" y="431.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P3.3/A15"/>
<wire x1="-477.52" y1="314.96" x2="-530.86" y2="314.96" width="0.1524" layer="91"/>
<label x="-530.86" y="314.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="KP2" class="0">
<segment>
<pinref part="KP" gate="G$1" pin="2"/>
<wire x1="-805.18" y1="464.82" x2="-815.34" y2="464.82" width="0.1524" layer="91"/>
<label x="-815.34" y="464.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P1.1/TA0.2/VEREF+"/>
<wire x1="-477.52" y1="327.66" x2="-530.86" y2="327.66" width="0.1524" layer="91"/>
<label x="-530.86" y="327.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="KP4" class="0">
<segment>
<pinref part="KP" gate="G$1" pin="4"/>
<wire x1="-802.64" y1="464.82" x2="-802.64" y2="469.9" width="0.1524" layer="91"/>
<wire x1="-802.64" y1="469.9" x2="-815.34" y2="469.9" width="0.1524" layer="91"/>
<label x="-815.34" y="469.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P3.0/A12"/>
<wire x1="-477.52" y1="322.58" x2="-530.86" y2="322.58" width="0.1524" layer="91"/>
<label x="-530.86" y="322.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="KP6" class="0">
<segment>
<pinref part="KP" gate="G$1" pin="6"/>
<wire x1="-800.1" y1="464.82" x2="-800.1" y2="474.98" width="0.1524" layer="91"/>
<wire x1="-800.1" y1="474.98" x2="-815.34" y2="474.98" width="0.1524" layer="91"/>
<label x="-815.34" y="474.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P3.2/A14"/>
<wire x1="-477.52" y1="317.5" x2="-530.86" y2="317.5" width="0.1524" layer="91"/>
<label x="-530.86" y="317.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U1" gate="B" pin="P4.7"/>
<wire x1="-477.52" y1="312.42" x2="-508" y2="312.42" width="0.1524" layer="91"/>
<wire x1="-508" y1="312.42" x2="-508" y2="304.8" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-508" y1="304.8" x2="-513.08" y2="304.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U1" gate="B" pin="PJ.5/LFXOUT"/>
<wire x1="-447.04" y1="355.6" x2="-447.04" y2="370.84" width="0.1524" layer="91"/>
<pinref part="XTA" gate="G$1" pin="1"/>
<wire x1="-447.04" y1="370.84" x2="-449.58" y2="370.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U1" gate="B" pin="PJ.4/LFXIN"/>
<wire x1="-444.5" y1="355.6" x2="-444.5" y2="375.92" width="0.1524" layer="91"/>
<pinref part="XTA" gate="G$1" pin="2"/>
<wire x1="-444.5" y1="375.92" x2="-449.58" y2="375.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U1" gate="B" pin="P2.7"/>
<wire x1="-426.72" y1="355.6" x2="-426.72" y2="365.76" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-426.72" y1="365.76" x2="-408.94" y2="365.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EXTRA" class="0">
<segment>
<pinref part="KP" gate="G$1" pin="8"/>
<wire x1="-797.56" y1="464.82" x2="-782.32" y2="464.82" width="0.1524" layer="91"/>
<label x="-782.32" y="464.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="P1.4/TB0.1"/>
<wire x1="-477.52" y1="307.34" x2="-502.92" y2="307.34" width="0.1524" layer="91"/>
<wire x1="-502.92" y1="307.34" x2="-502.92" y2="299.72" width="0.1524" layer="91"/>
<label x="-502.92" y="299.72" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
