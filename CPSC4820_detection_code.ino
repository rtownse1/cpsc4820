/*
 Robert Townsend
 CPSC 4820 Fall 2015
 Detector Code
 */

// Pin definitions
#define led RED_LED
#define led2 P4_7
#define alarm P4_0

// Constants
const unsigned long t = 100;


/*
  Main Code
 */
void setup()
{
  // Define pin modes
  pinMode(led, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(alarm, INPUT);
  Serial.begin(9600);
  Serial.println("READY\n");
}

void loop()
{
  Serial.println(digitalRead(alarm));
  digitalWrite(led, LOW);
  digitalWrite(led2, LOW);
  if (digitalRead(alarm) == LOW) {
    // triggered
    digitalWrite(led, HIGH);
    digitalWrite(led2, HIGH);
    delay(t);
  }
  else {
    digitalWrite(led, LOW);
    digitalWrite(led, LOW);
    delay(t);
  }
}



